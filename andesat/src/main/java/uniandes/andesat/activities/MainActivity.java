package uniandes.andesat.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

import uniandes.andesat.R;
import uniandes.andesat.database.PersistenceManager;
import uniandes.andesat.logic.AndeSAT;
import uniandes.andesat.logic.Driver;
import uniandes.andesat.logic.Passenger;
import uniandes.andesat.logic.Trip;

public class MainActivity extends Activity {

    private ImageButton btnDriver;

    private ImageButton btnPassenger;

    private AlertDialog alert;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Passenger passenger = PersistenceManager.getInstance(getApplicationContext()).getUser();
        Driver driver = PersistenceManager.getInstance(getApplicationContext()).getDriver();


        if(passenger == null)
        {
            Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
            startActivity(intent);
        }
        else
        {
            AndeSAT.getInstance().setPassenger(passenger);
            try {
                Trip[] trips =PersistenceManager.getInstance(getApplicationContext()).getTrips(passenger);
                if(trips!=null)
                {
                    for(Trip t:trips)
                    {
                        AndeSAT.getInstance().addTrip(t);
                    }
                }
            } catch (Exception e) {
                createDialog("Error",e.getMessage());
            }
        }
        if(driver != null)
        {
            AndeSAT.getInstance().setDriver(driver);
        }
        btnDriver = (ImageButton)findViewById(R.id.btnDriver);

        btnDriver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(AndeSAT.getInstance().getDriver() == null)
                {
                    createCustomDialog();
                }
                else
                {
                    Intent intent = new Intent(getApplicationContext(), DriverMenuActivty.class);
                    startActivity(intent);
                }
            }
        });
        btnPassenger = (ImageButton)findViewById(R.id.btnPassenger);
        btnPassenger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), PassMenuActivty.class);
                startActivity(intent);
            }
        });


    }

    private void createCustomDialog()
    {

        LayoutInflater inflater = this.getLayoutInflater();
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final View layout = inflater.inflate(R.layout.driver_data_dialog, null);


        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(layout);
                // Add action buttons

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                EditText txtModel = (EditText) layout.findViewById(R.id.txtCarModelD);
                EditText txtPlates = (EditText) layout.findViewById(R.id.txtCarPlatesD);
                if (txtModel.getText().toString() == null
                        || txtModel.getText().toString() == null || txtModel.getText().toString().equals("") || txtPlates.getText()
                        .toString().equals("")) {
                    builder.setTitle("Fill all the fields.");
                } else {
                    Driver driver = new Driver();
                    driver.setName(AndeSAT.getInstance().getPassenger().getName());
                    driver.setPhoneNumber(AndeSAT.getInstance().getPassenger().getPhoneNumber());
                    driver.setEmail(AndeSAT.getInstance().getPassenger().getEmail());
                    driver.setCarPlates(txtPlates.getText().toString());
                    driver.setCarModel(txtModel.getText().toString());
                    try {
                        AndeSAT.getInstance().setDriver(driver);
                        PersistenceManager.getInstance(getApplicationContext()).createDriver(driver);
                        alert.dismiss();
                        Intent intent = new Intent(getApplicationContext(), DriverMenuActivty.class);
                        startActivity(intent);

                    }
                    catch (Exception e)
                    {
                        alert.dismiss();
                        createDialog("Error", e.getMessage());
                    }


                }
            }
        });

        alert = builder.create();
        alert.show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    private void createDialog(String title, String message )
    {
        AlertDialog dialog;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(true);
        dialog = builder.create();
        dialog.show();
    }
    
}
