package uniandes.andesat.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import uniandes.andesat.R;
import uniandes.andesat.database.PersistenceManager;
import uniandes.andesat.logic.AndeSAT;
import uniandes.andesat.logic.Trip;

public class NotifyBoardingActivity extends Activity {

    private Spinner spTrip;

    private Button btnNotify;

    private Trip[] trips;

    private AlertDialog dialog;

    private Trip pickedTrip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notify_boarding);
        btnNotify = (Button)findViewById(R.id.btnNotifyPass);
        btnNotify.setEnabled(false);
        btnNotify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    AndeSAT.getInstance().joinTrip(pickedTrip);
                    createDialog("Thanks!", "The other passengers will be notified");
                } catch (Exception e) {
                    createDialog("Error", e.getMessage());
                }
            }
        });

        spTrip = (Spinner)findViewById(R.id.spTrip);
        try
        {
            trips = PersistenceManager.getInstance(getApplicationContext()).getTrips(AndeSAT.getInstance().getPassenger());
            String[] tripDates = new String[trips.length];
            int i = 0;
            DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            for(Trip t: trips)
            {
                tripDates[i] = format.format(t.getStartingTime());
                i++;
            }
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                    R.layout.spinner_item, tripDates);
            spTrip.setAdapter(adapter);
        }
        catch (Exception e)
        {
            createDialog("Error", e.getMessage());
        }
        spTrip.setOnItemSelectedListener( new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                pickedTrip = trips[i];
                btnNotify.setEnabled(true);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                if(trips.length>0)
                {
                    btnNotify.setEnabled(true);
                }
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.notify_boarding, menu);
        return true;
    }

    private void createDialog(String title, String message )
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(true);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialog.dismiss();
            }
        });
        dialog = builder.create();
        dialog.show();
    }
    
}
