package uniandes.andesat.activities;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import uniandes.andesat.R;
import uniandes.andesat.database.PersistenceManager;
import uniandes.andesat.logic.AndeSAT;
import uniandes.andesat.logic.Driver;
import uniandes.andesat.logic.Passenger;

public class RegisterActivity extends Activity {

    private EditText txtName, txtPNumber, txtEmail, txtCarModel, txtCarPlates;

    private Button btnContinue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_layout);
        txtName = (EditText)findViewById(R.id.txtName);
        txtPNumber = (EditText)findViewById(R.id.txtPNumber);
        txtEmail = (EditText)findViewById(R.id.txtMail);
        txtCarModel = (EditText)findViewById(R.id.txtCarMOdel);
        txtCarPlates = (EditText)findViewById(R.id.txtCarPlates);
        btnContinue = (Button)findViewById(R.id.btnContinue);
        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(txtName.getText().toString().equals("")||txtPNumber.getText().toString().equals("")
                        ||txtEmail.getText().toString().equals(""))
                {
                    createDialog("Sign Up Error","You must complete all compulsory fields.");
                }
                else if(!txtPNumber.getText().toString().equals("") && !txtPNumber.getText().toString().replaceAll("[0-9]+","").equals(""))
                    {
                    createDialog("Phone Number Error", "The phone must be a number!");
                }
                else if(txtPNumber.getText().toString().length()!=10)
                {
                    createDialog("Phone Number Error", "The number must be a cell number.");
                }
                else
                {
                    try
                    {
                        Passenger passenger = new Passenger();
                        passenger.setName(txtName.getText().toString());
                        passenger.setPhoneNumber(txtPNumber.getText().toString());
                        passenger.setEmail(txtEmail.getText().toString());
                        Driver driver = null;
                        if(!txtCarModel.getText().toString().equals("") && !txtCarPlates.getText().toString().equals(""))
                        {
                            driver = new Driver();
                            driver.setName(txtName.getText().toString());
                            driver.setPhoneNumber(txtPNumber.getText().toString());
                            String email = txtEmail.getText().toString().concat("@uniandes.edu.co");
                            driver.setEmail(email);
                            driver.setCarPlates(txtCarPlates.getText().toString());
                            driver.setCarModel(txtCarModel.getText().toString());
                            AndeSAT.getInstance().setDriver(driver);
                        }
                        AndeSAT.getInstance().setPassenger(passenger);
                        PersistenceManager.getInstance(getApplicationContext()).createUser(passenger, driver);
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    catch (Exception e)
                    {
                        createDialog("User Registration Error", e.getMessage());
                    }
                }


            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.register, menu);
        return true;
    }

    private void createDialog(String title, String message )
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        AlertDialog dialog = builder.create();
        dialog.show();
    }
    
}
