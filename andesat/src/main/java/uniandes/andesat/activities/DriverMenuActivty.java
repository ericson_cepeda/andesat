package uniandes.andesat.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

import uniandes.andesat.R;
import uniandes.andesat.database.PersistenceManager;
import uniandes.andesat.logic.AndeSAT;

public class DriverMenuActivty extends Activity {

    private Button btnAddRoute;

    private Button btnSchTrip;

    private Button btnStartTrip;

    private AlertDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.driver_menu);
        btnAddRoute = (Button)findViewById(R.id.btnAddRoute);
        btnAddRoute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), AddRouteActivity.class);
                startActivity(intent);
            }
        });

        btnSchTrip = (Button)findViewById(R.id.btnSchTrip);
        btnSchTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), ScheduleTripActivity.class);
                startActivity(intent);
            }
        });
        try
        {
            if(PersistenceManager.getInstance(getApplicationContext()).getRoutes(AndeSAT.getInstance().getDriver()) == null )
            {
                btnSchTrip.setEnabled(false);
            }
            else
            {
                btnSchTrip.setEnabled(true);
            }

        }
        catch (Exception e)
        {
            createDialog("Error", e.getMessage());
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.driver_menu_activty, menu);
        return true;
    }

    private void createDialog(String title, String message )
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(true);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialog.dismiss();
            }
        });
        dialog = builder.create();
        dialog.show();
    }


}
