package uniandes.andesat.activities;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.provider.CalendarContract;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import uniandes.andesat.R;
import uniandes.andesat.database.PersistenceManager;
import uniandes.andesat.logic.AndeSAT;
import uniandes.andesat.logic.Driver;
import uniandes.andesat.logic.Passenger;
import uniandes.andesat.logic.Route;

public class ScheduleTripActivity extends Activity {

    private static final int DATE_DIALOG_ID = 1000;
    private static final int TIME_DIALOG_ID = 2000;
    private Spinner spPickRoute;

    private Button btnSetDateTime;

    private Button btnAddPassengers;

    private Button btnSTOk;

    private Button btnSetTime;

    private AlertDialog dialog;

    private int year;

    private int month;

    private int day;

    private int hour;

    private int minute;

    private Date newDate;

    private Route pickedRoute;

    private Route[] routes;

    private Passenger[] passengers;

    private ArrayList<Passenger> ps;

    private AlertDialog alert;



    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,int selectedMonth, int selectedDay) {
            year = selectedYear;
            month = selectedMonth;
            day = selectedDay;
            btnSetDateTime.setText(day+"/"+(month+1)+"/"+year);
            newDate = new Date((year-1900), month, day);
        }
    };

    private TimePickerDialog.OnTimeSetListener timePickerListener =
            new TimePickerDialog.OnTimeSetListener() {
                public void onTimeSet(TimePicker view, int selectedHour,
                                      int selectedMinute) {
                    hour = selectedHour;
                    minute = selectedMinute;
                    btnSetTime.setText(hour+":"+minute);

                }
            };

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sch_trip_activty);

        spPickRoute = (Spinner)findViewById(R.id.spRoute);
        try
        {
            routes = PersistenceManager.getInstance(getApplicationContext()).getRoutes(AndeSAT.getInstance().getDriver());
            String[] routeNames = new String[routes.length];
            int i = 0;
            for(Route r: routes)
            {
                routeNames[i] = r.getName();
                i++;
            }
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                    R.layout.spinner_item, routeNames);
            spPickRoute.setAdapter(adapter);
            ps = new ArrayList<Passenger>();
        }
        catch (Exception e)
        {
            createDialog("Error", e.getMessage());
        }
        spPickRoute.setOnItemSelectedListener( new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                pickedRoute = routes[i];
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        btnSetDateTime = (Button)findViewById(R.id.btnTripDate);
        btnSetDateTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog(DATE_DIALOG_ID);
            }
        });

        btnSetTime = (Button)findViewById((R.id.btnTripTime));
        btnSetTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog(TIME_DIALOG_ID);
            }
        });
        btnAddPassengers = (Button)findViewById(R.id.btnAddPass);
        btnAddPassengers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createCustomDialog();
            }
        });

        btnSTOk = (Button)findViewById(R.id.btnSTOk);
        btnSTOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                passengers = new Passenger[ps.size()];
                for(int i = 0; i < ps.size(); i++)
                {
                    passengers[i] = ps.get(i);
                }
                DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                try
                {
                    String format;
                    if(hour < 10)
                    {
                        if(minute < 10)
                        {
                            format = day+"/"+month+"/"+year+" 0"+hour+":0"+minute;
                        }
                        else
                        {
                            format = day+"/"+month+"/"+year+" 0"+hour+":"+minute;
                        }

                    }
                    else
                    {
                        if(minute < 10)
                        {
                            format = day+"/"+month+"/"+year+" "+hour+":0"+minute;
                        }
                        else
                        {
                            format = day+"/"+month+"/"+year+" "+hour+":"+minute;
                        }
                    }
                    Date tripDate = formatter.parse(format);
                    if(passengers.length==0)
                    {
                        throw new Exception("You must add at least one passenger.");
                    }
                    long hourMillis = hour*3600*1000;
                    long minMillis = minute*60*1000;
                    long timeMillis = hourMillis + minMillis;
                    long endTImeMillis = pickedRoute.getApproxLength()*60*1000 + timeMillis;
                    long millis = tripDate.getTime();
                    millis+= endTImeMillis;
                    Date eDate = new Date(millis);
                    PersistenceManager.getInstance(getApplicationContext()).createTrip(pickedRoute.getName(),tripDate, eDate,
                            AndeSAT.getInstance().getDriver(), passengers);
                    AndeSAT.getInstance().createTrip(pickedRoute.getName(), passengers, tripDate);
                    Intent intent = new Intent(Intent.ACTION_INSERT)
                            .setType("vnd.android.cursor.item/event")
                            .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, timeMillis)
                            .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endTImeMillis)
                            .putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY , false)
                            .putExtra(CalendarContract.Events.TITLE, "New AndeSAT trip")
                            .putExtra(CalendarContract.Events.DESCRIPTION, "Carpooling trip scheduled with AndeSAT")
                            .putExtra(CalendarContract.Events.EVENT_LOCATION, pickedRoute.getSource())
                            .putExtra(CalendarContract.Events.AVAILABILITY, CalendarContract.Events.AVAILABILITY_BUSY)
                            .putExtra(CalendarContract.Events.ACCESS_LEVEL, CalendarContract.Events.ACCESS_PRIVATE);
                    startActivity(intent);
                    finish();
                }
                catch (Exception e)
                {
                    createDialog("Error", e.getMessage());
                }
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.schedule_trip, menu);
        return true;
    }

    private void createDialog(String title, String message )
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(true);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialog.dismiss();
            }
        });
        dialog = builder.create();
        dialog.show();
    }

    @Override
    protected Dialog onCreateDialog(int id)
    {
        Calendar c = Calendar.getInstance();
        switch (id)
        {
            case DATE_DIALOG_ID:

                year = c.get(Calendar.YEAR);
                month = c.get(Calendar.MONTH);
                day = c.get(Calendar.DAY_OF_MONTH);
                return new DatePickerDialog(this, datePickerListener, year, month, day);
            case TIME_DIALOG_ID:
                hour = c.get(Calendar.HOUR_OF_DAY);
                minute = c.get(Calendar.MINUTE);
                return new TimePickerDialog(this,
                        timePickerListener, hour, minute,false);
        }
        return null;
    }

    private void createCustomDialog()
    {

        LayoutInflater inflater = this.getLayoutInflater();
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final View layout = inflater.inflate(R.layout.dialog_add_passenger, null);


        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(layout);
        // Add action buttons

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                EditText txtPNameADP = (EditText) layout.findViewById(R.id.txtPNameAPD);
                EditText txtPPhoneADP = (EditText) layout.findViewById(R.id.txtPPhoneAPD);
                if (txtPNameADP.getText().toString() == null
                        || txtPNameADP.getText().toString() == null || txtPNameADP.getText().toString().equals("") || txtPPhoneADP.getText()
                        .toString().equals("")) {
                    builder.setTitle("Fill all the fields.");
                } else {
                    Passenger passenger = new Passenger();
                    passenger.setPhoneNumber(txtPPhoneADP.getText().toString());
                    passenger.setName(txtPNameADP.getText().toString());
                    try {
                        ps.add(passenger);
                        createDialog("Success", "Passenger " + passenger.getName()+ " added.");
                        alert.dismiss();
                    }
                    catch (Exception e)
                    {
                        alert.dismiss();
                        createDialog("Error", e.getMessage());
                    }


                }
            }
        });

        alert = builder.create();
        alert.show();
    }



    
}
