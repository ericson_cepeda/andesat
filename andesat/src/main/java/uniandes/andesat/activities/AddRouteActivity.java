package uniandes.andesat.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ToggleButton;

import uniandes.andesat.R;
import uniandes.andesat.database.PersistenceManager;
import uniandes.andesat.logic.AndeSAT;
import uniandes.andesat.logic.Driver;
import uniandes.andesat.logic.Route;

public class AddRouteActivity extends Activity {
    private EditText txtRName;

    private EditText txtApprLength;

    private ToggleButton tgToUniversity;

    private EditText txtSource;

    private EditText txtEnd;

    private Button btnSave;

    private AlertDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_route_activity);
        txtRName = (EditText)findViewById(R.id.txtRName);
        txtApprLength = (EditText)findViewById(R.id.txtApprLength);
        txtSource = (EditText)findViewById(R.id.txtSource);
        txtEnd = (EditText)findViewById(R.id.txtEnd);
        tgToUniversity = (ToggleButton)findViewById(R.id.tgToUniversity);
        tgToUniversity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean on = tgToUniversity.isChecked();
                if(on)
                {
                    txtSource.setText("");
                    txtSource.setEnabled(true);
                    txtSource.setHint("Address from start");
                    txtEnd.setText(R.string.strUniversity);
                    txtEnd.setEnabled(false);
                }
                else
                {
                    txtSource.setText(R.string.strUniversity);
                    txtSource.setEnabled(false);
                    txtEnd.setText("");
                    txtEnd.setEnabled(true);
                    txtEnd.setHint(R.string.strAddressDest);
                }
            }
        });

        btnSave = (Button)findViewById(R.id.btnSaveR);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(txtRName.getText().toString().equals("")||txtSource.getText().toString().equals("")
                        || txtEnd.getText().toString().equals("")||
                        txtApprLength.getText().toString().equals(""))
                {
                    createDialog("Error", "All the fields must be filled.");
                }
                else
                {
                    String appLengthS = txtApprLength.getText().toString();
                    final int appLength= Integer.parseInt(appLengthS);

                    Route route = new Route(txtRName.getText().toString(), appLength,
                            txtSource.getText().toString(), txtEnd.getText().toString());
                    Driver driver = AndeSAT.getInstance().getDriver();
                    try
                    {
                        PersistenceManager.getInstance(getApplicationContext()).addRoute(driver, route);
                        AndeSAT.getInstance().addRoute(route);
                        createDialog("Add Route", "Route added succesfully.");
                        finish();
                    }
                    catch (Exception e)
                    {
                        createDialog("Error", e.getMessage());
                    }


                }
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.add_route, menu);
        return true;
    }

    private void createDialog(String title, String message )
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(true);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialog.dismiss();
            }
        });
        dialog = builder.create();
        dialog.show();
    }
    
}
