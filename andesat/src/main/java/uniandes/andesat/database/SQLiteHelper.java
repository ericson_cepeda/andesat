package uniandes.andesat.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by User on 9/15/13.
 */
public class SQLiteHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "andesat.db";

    protected static final String PRIMARY_ID = "id";

    protected static final String USER_TABLE = "users";
    protected static final String COLUMN_MAIL = "mail";
    protected static final String COLUMN_USER_NAME = "name";
    protected static final String COLUMN_PHONE = "phone";
    protected static final String DRIVER_ID = "driver_id";

    protected static final String DRIVER_TABLE = "drivers";
    protected static final String COLUMN_MODEL = "model";
    protected static final String COLUMN_PLATES = "plates";

    protected static final String ROUTE_TABLE = "routes";
    protected static final String COLUMN_NAME = "name";
    protected static final String COLUMN_LENGTH = "length";
    protected static final String COLUMN_SOURCE = "source";
    protected static final String COLUMN_END = "end";

    protected static final String TRIP_TABLE = "trips";
    protected static final String COLUMN_STARTING_TIME = "starting_time";
    protected static final String COLUMN_FINISH_TIME = "finish_time";
    protected static final String COLUMN_ROUTE_ID = "route_id";
    protected static final String COLUMN_DRIVER_ID = "driver_id";

    protected static final String TRIP_USER_TABLE = "users_trips";
    protected static final String COLUMN_USER_ID = "user_id";
    protected static final String COLUMN_TRIP_ID = "trip_id";

    private final String CREATE_TABLE = "create table if not exists %s ( %s )";
    private static final String DROP_TABLE = "drop table if not exists %s";

    public SQLiteHelper(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String tableValues;

        tableValues = PRIMARY_ID+" integer primary key autoincrement, "+COLUMN_MODEL+" text, "+COLUMN_PLATES+" text unique";
        String createTable = "create table if not exists "+DRIVER_TABLE+" ("+tableValues+")";
        sqLiteDatabase.execSQL(createTable);

        tableValues = PRIMARY_ID + " integer primary key autoincrement, "+ COLUMN_MAIL+" text unique, " +
                COLUMN_USER_NAME+" text, "+COLUMN_PHONE+" text unique, "+DRIVER_ID+" integer, FOREIGN" +
                " KEY("+DRIVER_ID+") references "+DRIVER_TABLE+"("+PRIMARY_ID+")";

        createTable = "create table if not exists "+USER_TABLE+" ("+tableValues+")";
        Log.d("AndeSAT DB", createTable);
        sqLiteDatabase.execSQL(createTable);

        tableValues = PRIMARY_ID + " integer primary key autoincrement, "+ COLUMN_NAME+ " text unique, "
                + COLUMN_LENGTH + " integer, " + COLUMN_SOURCE + " text, "+ COLUMN_END +" text, "
                + DRIVER_ID+ " integer, FOREIGN KEY("+DRIVER_ID+") references "+DRIVER_TABLE
                +"("+PRIMARY_ID+")";

        createTable = "create table if not exists "+ROUTE_TABLE+" ("+tableValues+")";
        sqLiteDatabase.execSQL(createTable);

        tableValues = PRIMARY_ID + " integer primary key autoincrement, "+  COLUMN_STARTING_TIME
                + " text, " + COLUMN_FINISH_TIME + " text, "+COLUMN_ROUTE_ID+" integer, "
                + COLUMN_DRIVER_ID + " integer, " + " FOREIGN KEY("+COLUMN_ROUTE_ID+")"
                + "references "+ROUTE_TABLE+"("+PRIMARY_ID+"), "+ "FOREIGN KEY("+COLUMN_DRIVER_ID+") "
                + "references "+DRIVER_TABLE+"("+PRIMARY_ID+")";

        createTable = "create table if not exists "+TRIP_TABLE+" ("+tableValues+")";
        sqLiteDatabase.execSQL(createTable);

        tableValues = COLUMN_TRIP_ID +" integer, "+COLUMN_USER_ID+" integer, FOREIGN KEY("+COLUMN_TRIP_ID
                +") REFERENCES "+TRIP_TABLE+" ("+PRIMARY_ID+"), FOREIGN KEY("+COLUMN_USER_ID
                +") REFERENCES "+USER_TABLE+" ("+PRIMARY_ID+")";
        createTable = "create table if not exists "+TRIP_USER_TABLE+" ("+tableValues+")";
        sqLiteDatabase.execSQL(createTable);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {
        Log.w(SQLiteHelper.class.getName(), "Upgrading database from version " + i + " to " + i2 + ", which will destroy all old data");
        sqLiteDatabase.execSQL(DROP_TABLE.format(DRIVER_TABLE));
        sqLiteDatabase.execSQL(DROP_TABLE.format(USER_TABLE));
        sqLiteDatabase.execSQL(DROP_TABLE.format(ROUTE_TABLE));
        sqLiteDatabase.execSQL(DROP_TABLE.format(TRIP_TABLE));
        sqLiteDatabase.execSQL(DROP_TABLE.format(TRIP_USER_TABLE));

    }
}
