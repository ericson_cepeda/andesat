package uniandes.andesat.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import uniandes.andesat.logic.Driver;
import uniandes.andesat.logic.Passenger;
import uniandes.andesat.logic.Route;
import uniandes.andesat.logic.Trip;
import uniandes.andesat.logic.User;


/**
 * Created by User on 9/15/13.
 */
public class PersistenceManager {

    private SQLiteHelper helper;

    private SQLiteDatabase db;

    private static PersistenceManager instance;

    public PersistenceManager(Context context)
    {
        helper = new SQLiteHelper(context);
    }

    public static PersistenceManager getInstance(Context context)
    {
        if(instance == null )
        {
            instance = new PersistenceManager(context);
        }
        return instance;
    }

    /**
     * Gets the main user of the application.
     * @return passenger. Every user in the system is a passenger by default. this method returns
     * the application main user.
     */
    public Passenger getUser()
    {
        Passenger passenger = null;
        try
        {
            String name;
            String phoneNumber;
            db = helper.getReadableDatabase();
            Cursor cursor = db.query(SQLiteHelper.USER_TABLE, new String[]{
                    SQLiteHelper.COLUMN_NAME, SQLiteHelper.COLUMN_PHONE},null, null, null ,null, null);
            if(cursor.moveToFirst())
            {
                name = cursor.getString(0);
                phoneNumber = cursor.getString(1);
                passenger= new Passenger();
                passenger.setPhoneNumber(phoneNumber);
                passenger.setName(name);
            }
            db.close();

        }
        catch (Exception e)
        {

        }
        return passenger;
    }

    public Driver getDriver()
    {
        Driver driver = null;
        try
        {
            int driverId = -1;
            String name;
            String phoneNumber;
            db = helper.getReadableDatabase();
            Cursor cursor = db.query(SQLiteHelper.DRIVER_TABLE, new String[]{SQLiteHelper.PRIMARY_ID,
            SQLiteHelper.COLUMN_PLATES, SQLiteHelper.COLUMN_MODEL}, null, null, null, null, null);
            if(cursor.moveToFirst())
            {
                driver = new Driver();
                driver.setCarPlates(cursor.getString(1));
                driver.setCarModel(cursor.getString(2));
                driverId = cursor.getInt(0);
            }
            else
            {
                db.close();
                cursor = null;
                throw new Exception("There are no drivers");

            }

            cursor = db.query(SQLiteHelper.USER_TABLE, new String[]{
                    SQLiteHelper.COLUMN_NAME, SQLiteHelper.COLUMN_PHONE},SQLiteHelper.DRIVER_ID
                    +" = "+driverId, null, null ,null, null);
            if(cursor.moveToFirst())
            {
                name = cursor.getString(0);
                phoneNumber = cursor.getString(1);
                driver.setPhoneNumber(phoneNumber);
                driver.setName(name);
            }

            cursor = db.query(SQLiteHelper.ROUTE_TABLE, new String[]{SQLiteHelper.COLUMN_NAME,
                SQLiteHelper.COLUMN_LENGTH, SQLiteHelper.COLUMN_SOURCE, SQLiteHelper.COLUMN_END},
                SQLiteHelper.COLUMN_DRIVER_ID+" = "+driverId, null, null, null, null);
            Route r;
            while (cursor.moveToNext())
            {
                 r = new Route(cursor.getString(0), cursor.getInt(1),cursor.getString(2)
                         ,cursor.getString(3));
                driver.addRoute(r);
            }
            db.close();

        }
        catch (Exception e)
        {
            Log.d("AndeSATDB", e.getMessage());
        }
        return driver;
    }

    public void createUser(Passenger passenger, Driver driver) throws Exception
    {
        int driverId;
        try
        {
            if(userExists(passenger))
            {
                throw new Exception("The user already exist.");
            }
            else
            {

                ContentValues values = new ContentValues();
                values.put(SQLiteHelper.COLUMN_NAME, passenger.getName());
                values.put(SQLiteHelper.COLUMN_PHONE, passenger.getPhoneNumber());
                values.put(SQLiteHelper.COLUMN_MAIL, passenger.getEmail());
                if(driver != null)
                {
                    db = helper.getWritableDatabase();
                    ContentValues driverValues = new ContentValues();
                    driverValues.put(SQLiteHelper.COLUMN_MODEL, driver.getCarModel());
                    driverValues.put(SQLiteHelper.COLUMN_PLATES, driver.getCarPlates());
                    db.insert(SQLiteHelper.DRIVER_TABLE, null , driverValues);
                    driverValues = null;
                    db.close();
                    db = helper.getReadableDatabase();
                    Cursor cursor = db.query(SQLiteHelper.DRIVER_TABLE, new String[]{SQLiteHelper.PRIMARY_ID}
                    ,SQLiteHelper.COLUMN_PLATES+ " LIKE '"+driver.getCarPlates()+"'", null, null, null, null);
                    if(cursor.moveToFirst())
                    {
                        driverId = cursor.getInt(0);
                        values.put(SQLiteHelper.DRIVER_ID, driverId);
                    }
                    cursor = null;
                    db.close();
                }
                db = helper.getWritableDatabase();
                db.insert(SQLiteHelper.USER_TABLE, null , values);
                values = null;
                db.close();
            }


        }catch (Exception e)
        {

            Log.e("AndeSAT DB", e.getMessage());
            e.printStackTrace();
            throw new Exception("Error adding user: "+ e.getMessage());
        }
    }

    /**
     * Checks if the given user already exist in the system.
     * @param user
     * @return
     * @throws Exception
     */
    public boolean userExists(User user) throws Exception
    {
        boolean exists = false;
        try
        {
            db = helper.getReadableDatabase();
            Cursor cursor = db.query(SQLiteHelper.USER_TABLE, new String[]{SQLiteHelper.COLUMN_NAME,
                    SQLiteHelper.COLUMN_PHONE, SQLiteHelper.COLUMN_MAIL}, SQLiteHelper.COLUMN_NAME+" LIKE '"+user.getName()
                    +"' AND "+ SQLiteHelper.COLUMN_PHONE + " LIKE '"+user.getPhoneNumber()+"' AND "+ SQLiteHelper.COLUMN_MAIL
                    +" LIKE '"+user.getEmail()+"'" ,null, null, null, null);
            exists = cursor.moveToFirst();
            db.close();
            cursor = null;
        }
        catch (Exception e)
        {
            Log.e("AndeSAT DB", e.getLocalizedMessage());
            e.printStackTrace();
            throw new Exception("Error reading database: "+ e.getMessage());
        }
        return exists;
    }

    /**
     * Creates a new driver in the system.
     * @param driver
     * @throws Exception
     */
    public void createDriver(Driver driver) throws Exception
    {
        try {

            int driverId = getDriverId(driver.getCarPlates());
            int userId = getUserId(driver.getPhoneNumber());
            db = helper.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(SQLiteHelper.COLUMN_DRIVER_ID, driverId);
            db.update(SQLiteHelper.USER_TABLE, values, SQLiteHelper.PRIMARY_ID+" = "+userId, null);
            values = null;
            db.close();
        }
        catch (Exception e)
        {
            throw new Exception("Error creating the driver: "+e.getMessage());
        }
    }

    /**
     * Gets the id of the user identified by it's phone number
     * @param phoneNumber The number of the user to get the id.
     * @return id.
     * @throws Exception
     */
    private int getUserId(String phoneNumber) throws Exception
    {
        int id = -1;
        try {
            db = helper.getReadableDatabase();
            Cursor cursor = db.query(SQLiteHelper.USER_TABLE, new String[]{SQLiteHelper.PRIMARY_ID}
                    , SQLiteHelper.COLUMN_PHONE + " LIKE '"+phoneNumber+"'", null, null, null, null);
            if(cursor.moveToFirst())
            {
                id = cursor.getInt(0);
                cursor = null;
                db.close();
            }
            else
            {
                cursor = null;
                db.close();
                throw new Exception("The user with the specified number doesn't exist.");
            }

        }
        catch (Exception e)
        {
            throw new Exception("Error reading database: "+e.getMessage());
        }
        return id;
    }

    /**
     * Gets the id of the driver that registered the given plates in the system.
     * @param carPlates
     * @return
     * @throws Exception
     */
    private int getDriverId(String carPlates) throws Exception
    {
        int id = -1;
        try {
            db = helper.getReadableDatabase();
            Cursor cursor = db.query(SQLiteHelper.DRIVER_TABLE, new String[]{SQLiteHelper.PRIMARY_ID}
                    , SQLiteHelper.COLUMN_PLATES + " LIKE '"+carPlates+"'", null, null, null, null);
            if(cursor.moveToFirst())
            {
                id = cursor.getInt(0);
                cursor = null;
                db.close();
            }
            else
            {
                cursor = null;
                db.close();
                throw new Exception("The user with the specified number doesn't exist.");
            }

        }
        catch (Exception e)
        {
            throw new Exception("Error reading database: "+e.getMessage());
        }
        return id;
    }

    /**
     * Adds a route to the driver in the system.
     * @param driver The driver of the route.
     * @param route The route to add.
     * @throws Exception
     */
    public void addRoute(Driver driver, Route route) throws Exception
    {
        try
        {
            int driverId = getDriverId(driver.getCarPlates());
            if(routeExist(route))
            {
                throw new Exception("The route already exist.");
            }
            else
            {
                db = helper.getWritableDatabase();
                ContentValues values = new ContentValues();
                values.put(SQLiteHelper.COLUMN_NAME, route.getName());
                values.put(SQLiteHelper.COLUMN_LENGTH, route.getApproxLength());
                values.put(SQLiteHelper.COLUMN_SOURCE, route.getSource());
                values.put(SQLiteHelper.COLUMN_END, route.getEnd());
                values.put(SQLiteHelper.COLUMN_DRIVER_ID, driverId);
                db.insert(SQLiteHelper.ROUTE_TABLE, null, values);
                values = null;
                db.close();
            }

        }
        catch (Exception e)
        {
            throw new Exception("Error writing the database: "+e.getMessage());
        }

    }

    /**
     * Detects if a route already exist in the system.
     * @param route The route to search.
     * @return <b>True</b> if the route exist, <b>False</b> on the contrary.
     * @throws Exception
     */
    private boolean routeExist(Route route) throws Exception
    {
        boolean exist = false;
        try
        {
            db = helper.getReadableDatabase();
            Cursor cursor = db.query(SQLiteHelper.ROUTE_TABLE, new String[]{SQLiteHelper.PRIMARY_ID}
                    , SQLiteHelper.COLUMN_NAME + " LIKE '"+route.getName()+"'", null, null, null, null);
            exist = cursor.moveToFirst();
            cursor = null;
            db.close();
        }
        catch (Exception e)
        {
            throw new Exception("Error reading database: "+ e.getMessage());
        }
        return  exist;
    }

    /**
     * Gets all the routes for the specified driver.
     * @param driver The driver to get the routes from.
     * @return the array of routes.
     * @throws Exception
     */
    public Route[] getRoutes(Driver driver) throws Exception
    {
        Route[] routes = null;
        ArrayList<Route> r = new ArrayList<Route>();
        try
        {
            int driverId = getDriverId(driver.getCarPlates());
            db = helper.getReadableDatabase();
            Cursor cursor = db.query(SQLiteHelper.ROUTE_TABLE, new String[]{SQLiteHelper.PRIMARY_ID},
                    SQLiteHelper.COLUMN_DRIVER_ID+" = "+driverId, null, null, null, null);
            while(cursor.moveToNext())
            {
                int id = cursor.getInt(0);
                r.add(getRouteFromId(id));
            }
            cursor = null;

        }
        catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
        routes = new Route[r.size()];
        int i = 0;
        for(Route route: r)
        {
            routes[i] = route;
            i++;
        }
        r = null;
        return routes;
    }

    /**
     * Gets a route given it's id in the database.
     * @param id The id of the route.
     * @return route.
     * @throws Exception
     */
    private Route getRouteFromId(int id) throws Exception
    {
        Route route;
        try
        {
            db = helper.getReadableDatabase();
            Cursor cursor = db.query(SQLiteHelper.ROUTE_TABLE, new String[]{SQLiteHelper.COLUMN_NAME,
            SQLiteHelper.COLUMN_LENGTH, SQLiteHelper.COLUMN_SOURCE, SQLiteHelper.COLUMN_END},
                    SQLiteHelper.PRIMARY_ID+ " = "+id, null, null, null, null);
            if(cursor.moveToFirst())
            {
                route = new Route(cursor.getString(0), cursor.getInt(1), cursor.getString(2),
                        cursor.getString(3));
            }
            else
            {
                db.close();
                cursor = null;
                throw new Exception("The route doesn't exist.");
            }
            db.close();
            cursor = null;
        }
        catch (Exception e)
        {
            throw new Exception("Error reading the database: " + e.getMessage());
        }
        return route;
    }

    /**
     * Creates a trip in the database system.
     * @param routeName The name of the route.
     * @param tripDate the date the trip starts.
     * @param endingTime The date the trip ends.
     * @param driver the driver of the trip.
     * @param passengers the passengers that ride the car.
     */
    public void createTrip(String routeName, Date tripDate, Date endingTime, Driver driver, Passenger[] passengers)
    {
        try
        {
            int driverId = getDriverId(driver.getCarPlates());
            DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            String sDate =  format.format(tripDate);
            db = helper.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(SQLiteHelper.COLUMN_DRIVER_ID, driverId);
            int routeId = -1;
            if(routeName!=null)
            {
                routeId = getRouteId(routeName);
                values.put(SQLiteHelper.COLUMN_ROUTE_ID, routeId);
            }
            values.put(SQLiteHelper.COLUMN_STARTING_TIME, sDate);
            if(endingTime!=null)
            {
                String eDate = format.format(endingTime);
                values.put(SQLiteHelper.COLUMN_FINISH_TIME, eDate);
            }
            db.insert(SQLiteHelper.TRIP_TABLE, null, values);

            for(Passenger p: passengers)
            {
                if(getPassengerId(p.getPhoneNumber())==-1)
                {
                    values.clear();
                    values.put(SQLiteHelper.COLUMN_USER_NAME, p.getName());
                    values.put(SQLiteHelper.COLUMN_PHONE, p.getPhoneNumber());
                    db.insert(SQLiteHelper.USER_TABLE, null, values);
                }
            }
            db.close();
            db = helper.getReadableDatabase();
            int[] pIds = new int[passengers.length];
            int id, i;
            Cursor cursor;
            i = 0;
            for(Passenger p: passengers)
            {
                cursor = db.query(SQLiteHelper.USER_TABLE, new String[]{SQLiteHelper.PRIMARY_ID}
                        , SQLiteHelper.COLUMN_PHONE + " LIKE '"+p.getPhoneNumber()+"'", null, null, null, null);
                if(cursor.moveToFirst())
                {
                    id = cursor.getInt(0);
                    pIds[i] = id;
                }
                i++;
            }
            if(routeId != -1)
            {
                cursor = db.query(SQLiteHelper.TRIP_TABLE, new String[]{SQLiteHelper.PRIMARY_ID}
                        , SQLiteHelper.COLUMN_ROUTE_ID + " = " + routeId + " AND " + SQLiteHelper.COLUMN_DRIVER_ID
                        + " = " + driverId, null, null, null, null);
            }
            else
            {
                cursor = db.query(SQLiteHelper.TRIP_TABLE, new String[]{SQLiteHelper.PRIMARY_ID}
                        , SQLiteHelper.COLUMN_STARTING_TIME + " LIKE '"+sDate+"' AND "+SQLiteHelper.COLUMN_DRIVER_ID
                        + " = "+driverId, null, null, null, null);
            }
            if(cursor.moveToFirst())
            {
                id = cursor.getInt(0);
                db.close();
                db = helper.getWritableDatabase();
                for(int j: pIds)
                {
                    values.clear();
                    values.put(SQLiteHelper.COLUMN_TRIP_ID, id);
                    values.put(SQLiteHelper.COLUMN_USER_ID, j);
                    db.insert(SQLiteHelper.TRIP_USER_TABLE, null, values);
                }
            }
            db.close();

        }
        catch(Exception e)
        {

        }
    }


    private int getPassengerId(String phoneNumber) throws Exception
    {
        int id = -1;
        try {
            db = helper.getReadableDatabase();
            Cursor cursor = db.query(SQLiteHelper.USER_TABLE, new String[]{SQLiteHelper.PRIMARY_ID}
                    , SQLiteHelper.COLUMN_PHONE + " LIKE '"+phoneNumber+"'", null, null, null, null);
            if(cursor.moveToFirst())
            {
                id = cursor.getInt(0);
                cursor = null;
                db.close();
            }
            else
            {
                cursor = null;
                db.close();
                throw new Exception("The passenger with the specified number doesn't exist.");
            }

        }
        catch (Exception e)
        {
            throw new Exception("Error reading database: "+e.getMessage());
        }
        return id;
    }

    private int getRouteId(String routeName) throws Exception
    {
        int id = -1;
        try {
            db = helper.getReadableDatabase();
            Cursor cursor = db.query(SQLiteHelper.ROUTE_TABLE, new String[]{SQLiteHelper.PRIMARY_ID}
                    , SQLiteHelper.COLUMN_NAME + " LIKE '"+routeName+"'", null, null, null, null);
            if(cursor.moveToFirst())
            {
                id = cursor.getInt(0);
                cursor = null;
                db.close();
            }
            else
            {
                cursor = null;
                db.close();
                throw new Exception("The route with the specified name doesn't exist.");
            }

        }
        catch (Exception e)
        {
            throw new Exception("Error reading database: "+e.getMessage());
        }
        return id;
    }

    private void addDriver(Driver driver) throws Exception
    {
        try
        {
            if(getDriverId(driver.getCarPlates())!= -1)
            {
                db = helper.getWritableDatabase();
                ContentValues values = new ContentValues();
                values.put(SQLiteHelper.COLUMN_PLATES, driver.getCarPlates());
                values.put(SQLiteHelper.COLUMN_MODEL, driver.getCarModel());
                db.insert(SQLiteHelper.DRIVER_TABLE, null, values);
                values.clear();
                db.close();
                int driverId = getDriverId(driver.getCarPlates());
                db = helper.getWritableDatabase();
                values.put(SQLiteHelper.COLUMN_USER_NAME, driver.getName());
                values.put(SQLiteHelper.COLUMN_USER_NAME, driver.getPhoneNumber());
                values.put(SQLiteHelper.COLUMN_DRIVER_ID, driverId);
                db.insert(SQLiteHelper.USER_TABLE, null, values);
                values = null;
                db.close();
            }
        }
        catch (Exception e)
        {
            throw new Exception("Unable to write to database: "+e.getMessage());
        }
    }


    public Trip[] getTrips(Passenger passenger) throws Exception{
        Trip[] trips = null;
        ArrayList<Trip> t = new ArrayList<Trip>();
        try
        {
            int passengerId = getPassengerId(passenger.getPhoneNumber());
            db = helper.getReadableDatabase();
            Cursor cursor = db.query(SQLiteHelper.TRIP_USER_TABLE, new String[]{SQLiteHelper.COLUMN_TRIP_ID},
                    SQLiteHelper.COLUMN_USER_ID+" = "+passengerId, null, null, null, null);
            while(cursor.moveToNext())
            {
                int id = cursor.getInt(0);
                t.add(getTripFromId(id));
            }
            cursor = null;

        }
        catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
        trips = new Trip[t.size()];
        int i = 0;
        for(Trip trip: t)
        {
            trips[i] = trip;
            i++;
        }
        t = null;
        return trips;
    }

    private Trip getTripFromId(int tripId) throws Exception
    {
        Trip trip;
        try
        {
            db = helper.getReadableDatabase();
            Cursor cursor = db.query(SQLiteHelper.TRIP_USER_TABLE, new String[]{SQLiteHelper.COLUMN_USER_ID},
                    SQLiteHelper.COLUMN_TRIP_ID+" = "+tripId, null, null, null, null);
            ArrayList<Passenger> passengers = new ArrayList<Passenger>();
            Passenger passenger;
            while(cursor.moveToNext())
            {
                passenger = getPassengerFromId(cursor.getInt(0));
                passengers.add(passenger);
            }
            db = helper.getReadableDatabase();
            cursor = db.query(SQLiteHelper.TRIP_TABLE, new String[]{SQLiteHelper.COLUMN_STARTING_TIME,
                    SQLiteHelper.DRIVER_ID},
                    SQLiteHelper.PRIMARY_ID+ " = "+tripId, null, null, null, null);
            if(cursor.moveToFirst())
            {
                DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                Date date = format.parse(cursor.getString(0));
                Driver driver = getDriverFromId(cursor.getInt(1));
                Passenger[] ps = passengers.toArray(new Passenger[passengers.size()]);
                trip = new Trip(date, ps, driver.getName(), driver.getPhoneNumber(),
                        driver.getCarModel(), driver.getCarPlates());
            }
            else
            {
                db.close();
                cursor = null;
                throw new Exception("The trip doesn't exist.");
            }
            db.close();
            cursor = null;
        }
        catch (Exception e)
        {
            throw new Exception("Error reading the database: " + e.getMessage());
        }
        return trip;
    }

    private Driver getDriverFromId(int driverId) throws Exception
    {
        Driver driver = null;
        try
        {
            db = helper.getReadableDatabase();
            Cursor cursor = db.query(SQLiteHelper.DRIVER_TABLE, new String[]{SQLiteHelper.COLUMN_MODEL,
            SQLiteHelper.COLUMN_PLATES}, SQLiteHelper.PRIMARY_ID + " = "+driverId, null, null, null, null);
            if(cursor.moveToFirst())
            {
                driver = new Driver();
                driver.setCarPlates(cursor.getString(1));
                driver.setCarModel(cursor.getString(0));
            }
            else
            {
                db.close();
                throw new Exception("The driver does not exist.");
            }
            cursor = db.query(SQLiteHelper.USER_TABLE, new String[]{SQLiteHelper.COLUMN_USER_NAME, SQLiteHelper.COLUMN_PHONE},
                    SQLiteHelper.COLUMN_DRIVER_ID +" = "+driverId, null, null, null, null);
            if(cursor.moveToFirst())
            {
                driver.setName(cursor.getString(0));
                driver.setPhoneNumber(cursor.getString(1));
            }
            db.close();
            cursor = null;
        }
        catch (Exception e)
        {
            throw new Exception("Error reading the database: "+e.getMessage());
        }
        return driver;
    }

    private Passenger getPassengerFromId(int passengerId)throws Exception
    {
        Passenger passenger = null;
        try
        {
            db = helper.getReadableDatabase();
            Cursor cursor = db.query(SQLiteHelper.USER_TABLE, new String[]{SQLiteHelper.COLUMN_USER_NAME,
            SQLiteHelper.COLUMN_PHONE}, SQLiteHelper.PRIMARY_ID+" = "+passengerId, null, null, null,null);
            if(cursor.moveToFirst())
            {
                passenger = new Passenger();
                passenger.setName(cursor.getString(0));
                passenger.setPhoneNumber(cursor.getString(1));
            }
            else
            {
                throw new Exception("The passenger does not exist.");
            }
            db.close();
            cursor = null;
        }
        catch (Exception e)
        {
            throw new Exception("Error reading the database: "+e.getMessage());
        }
        return passenger;
    }

}
