package uniandes.andesat.logic;

import java.util.ArrayList;

/**
 * Created by User on 9/11/13.
 */
public class Passenger extends User
{

    /**
     * Creates an instance of the object.
     */
    public Passenger()
    {
        super();
    }

    @Override
    public boolean equals(Object o)
    {
        Passenger b = (Passenger)o;
        boolean equals = this.getName().equals(b.getName()) && this.getPhoneNumber().equals(b.getPhoneNumber());
        return equals;

    }

    /**
     * Called when the passenger enters the car, indicating the other passengers that he/she has
     * been picked up.
     * @param trip
     * @throws Exception
     */
    public void joinTrip(Trip trip) throws Exception
    {
        ArrayList<Passenger> othersAfter = new ArrayList<Passenger>();
        MessageSender sender = new MessageSender();
        Passenger p;
        int index = -1;
        for(int i = 0; i < trip.getPassengers().size(); i++)
        {
            p = trip.getPassengers().get(i);
            if(p.equals(this))
            {
                index = i;
                break;
            }
        }
        for(int i = 0; i < trip.getPassengers().size(); i++)
        {
            p = trip.getPassengers().get(i);
            if(!p.equals(this) && i > index)
            {
                othersAfter.add(p);
            }
        }

        for(Passenger p1:othersAfter)
        {
            sender.setMessageText("Sent From AndeSAT:\n" +
                    "I was picked up!\n" +
                    "Driver: "+trip.getDriverName()+"\n" +
                    "Number: "+trip.getDriverPhone()+"\n" +
                    "Car model: "+trip.getCarModel()+"\n" +
                    "Car plates: "+trip.getCarPlates()+"\n" +
                    "Be on time!");
            sender.setPhoneNumber(p1.getPhoneNumber());
            try {
                sender.sendMessage();
            }
            catch (Exception e)
            {
                throw new Exception("Error sending the message: "+e.getMessage());
            }
            sender = null;
        }




    }
}
