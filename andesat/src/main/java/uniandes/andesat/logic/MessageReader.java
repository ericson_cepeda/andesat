package uniandes.andesat.logic;

import android.content.Context;
import android.telephony.SmsMessage;
import android.telephony.TelephonyManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import uniandes.andesat.database.PersistenceManager;

/**
 * @author Julio Mendoza
 * Reads the messages captured by the SMSReceiver
 */
public class MessageReader {

    /**
     * The messages captured by the receiver.
     */
    private ArrayList<SmsMessage> messages;

    /**
     * The instance of the MessageReader.
     */
    private static MessageReader instance;

    /**
     * The sender of the message.
     */
    private String msg_from;

    /**
     * The body of the message.
     */
    private String msg_body;

    /**
     * Initalizes the class.
     */
    public MessageReader()
    {
        messages = new ArrayList<SmsMessage>();
    }



    /**
     * Get the current instance of the object.
     * @return instance. The instance of the object.
     */
    public static MessageReader getInstance()
    {
        if(instance == null)
        {
            instance = new MessageReader();
        }
        return instance;
    }



    /**
     * Determines if the message complies with the application format.
     * @return complies <b>True</b> if the message is written according with the format establishd. <b>False</b> on the contrary.
     */
    public boolean readMessage(Object pdu, Context context)
    {
        boolean complies = false;
        SmsMessage message = SmsMessage.createFromPdu((byte[])pdu);
        msg_from = message.getServiceCenterAddress();
        msg_body = message.getMessageBody();
        complies = msg_body.contains("Sent From AndeSAT:");
        if(complies)
        {
            if(msg_body.contains("We are going"))
            {
                BufferedReader br = new BufferedReader(new StringReader(msg_body));
                try {
                    String s;
                    String driverName = null, plates = null, model = null, date = null, name = null,
                            phone = null;
                    ArrayList<Passenger> passengers = new ArrayList<Passenger>();
                    while((s = br.readLine()) != null)
                    {
                        if(s.contains("Hi"))
                        {
                            name = s.split(": ")[1];
                        }
                        if(s.contains("I am:"))
                        {
                            driverName = s.split(": ")[1];
                        }
                        if(s.contains("My car is"))
                        {
                            model = s.split(": ")[1];
                        }
                        if(s.contains("The plates are"))
                        {
                            plates = s.split(": ")[1];
                        }
                        if(s.contains("At approximately"))
                        {
                            date = s.split(": ")[1];
                        }
                        if(s.contains("Partner"))
                        {
                            String x = s.split(": ")[1];
                            if(x.contains("After"))
                            {
                                String fields[] = x.split("|");
                                Passenger p = new Passenger();
                                p.setName(fields[0]);
                                p.setPhoneNumber(fields[1]);
                                passengers.add(p);
                            }
                        }
                    }
                    Passenger p = new Passenger();
                    p.setName(name);
                    TelephonyManager tMgr =(TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
                    phone = tMgr.getLine1Number();
                    p.setPhoneNumber(phone);
                    passengers.add(p);
                    DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                    Date d = format.parse(date);
                    Passenger[] ps = passengers.toArray(new Passenger[passengers.size()]);
                    Trip trip = new Trip(d, ps, driverName, msg_from, model, plates);
                    Driver driver = new Driver();
                    driver.setName(driverName);
                    driver.setPhoneNumber(msg_from);
                    driver.setCarModel(model);
                    driver.setCarPlates(plates);
                    PersistenceManager.getInstance(context).createTrip(null, d,null,driver,ps);
                    AndeSAT.getInstance().addTrip(trip);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return complies;
    }

    public String getMsg_from() {
        return msg_from;
    }

    public void setMsg_from(String msg_from) {
        this.msg_from = msg_from;
    }

    public String getMsg_body() {
        return msg_body;
    }

    public void setMsg_body(String msg_body) {
        this.msg_body = msg_body;
    }
}
