package uniandes.andesat.logic;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by User on 9/10/13.
 */
public class Trip {

    //----------------------------------------------------------------------------------------------
    //CONSTANTS
    //----------------------------------------------------------------------------------------------
    /**
     * Defines that the route ended.
     */
    private final static String ENDED = "Started";

    /**
     * Defines that the trip has started
     */
    private final static String STARTED = "Started";

    /**
     * Defines that the trip is pending to start.
     */
    private final static String IDLE = "Idle";

    /**
     * Defines that the trip was cancelled.
     */
    private final static String CANCELLED = "Cancelled";

    //----------------------------------------------------------------------------------------------
    //FIELDS
    //----------------------------------------------------------------------------------------------

    /**
     * The route the trip is travelling.
     */
    private Route associatedRoute;

    /**
     * State of the route.
     */
    private String state;

    /**
     * The date that the trip started.
     */
    private Date startingTime;

    /**
     * The date that the trip ended.
     */
    private Date endingTime;

    /**
     * The passenger invited to the trip.
     */
    private ArrayList<Passenger> passengers;

    /**
     * The name of driver of the trip.
     */
    private String driverName;

    /**
     * The phone number of the driver.
     */
    private String driverPhone;

    /**
     * The model of the trip car.
     */
    private String carModel;

    /**
     * the plates of the car.
     */
    private String carPlates;

    /**
     * Initializes an instance of an object.
     * @param sDate
     * @param list
     * @param driverName
     * @param driverPhone
     */
    public Trip(Date sDate, Passenger[] list, String driverName, String driverPhone, String carModel,
                String carPlates)
    {
        this.startingTime = sDate;
        this.passengers = new ArrayList<Passenger>();
        this.driverName = driverName;
        this.driverPhone = driverPhone;
        this.carModel = carModel;
        this.carPlates = carPlates;
        addPassengers(list);
    }

    /**
     * Adds passenger from list.
     * @param list
     */
    private void addPassengers(Passenger[] list)
    {
        for (int i = 0; i < list.length; i++)
        {
            this.passengers.add(list[i]);
        }
    }

    /**
     * Changes the associated route of the trip.
     * @param associatedRoute
     */
    public void setAssociatedRoute(Route associatedRoute) {
        this.associatedRoute = associatedRoute;
    }

    /**
     * Returns the list of passengers of the trip.
     * @return passengers.
     */
    public ArrayList<Passenger> getPassengers()
    {
        return passengers;
    }

    /**
     * Retruns the  name of the driver of the trip.
     * @return driverName.
     */
    public String getDriverName() {
        return driverName;
    }

    /**
     * Returns the driver's phone
     * @return driverPhone.
     */
    public String getDriverPhone() {
        return driverPhone;
    }

    /**
     * The model of the car.
     * @return
     */
    public String getCarModel() {
        return carModel;
    }

    /**
     * The plates of the car.
     * @return
     */
    public String getCarPlates() {
        return carPlates;
    }

    public Date getStartingTime()
    {
        return startingTime;
    }
}
