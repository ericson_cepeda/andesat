package uniandes.andesat.logic;

/**
 * This class represents a route in the system.
 */
public class Route {

    /**
     * The name of the route.
     */
    private String name;

    /**
     * The approximate length in minutes.
     */
    private int approxLength;

    /**
     * The beginning location of the route.
     */
    private String source;

    /**
     * The ending location of the route.
     */
    private String end;

    /**
     * Initializes the object.
     * @param name The name of the route.
     * @param approxLength The approximate length of the route.
     * @param source The starting opi
     * @param end
     */
    public Route(String name, int approxLength, String source, String end)
    {
        this.name = name;
        this.approxLength = approxLength;
        this.source = source;
        this.end = end;
    }

    /**
     * Returns the name of the route.
     * @return
     */
    public String getName() {
        return name;
    }

    public int getApproxLength() {
        return approxLength;
    }

    public String getSource() {
        return source;
    }

    public String getEnd() {
        return end;
    }

    @Override
    public boolean equals(Object o)
    {
        Route otherRoute = (Route)o;
        return this.getName().equals(otherRoute.getName())
                && this.approxLength == otherRoute.getApproxLength()
                && this.getSource().equals(otherRoute.getSource())
                && this.getEnd().equals(otherRoute.getEnd());
    }

}
