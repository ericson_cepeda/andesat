package uniandes.andesat.logic;


import android.telephony.SmsManager;
import android.telephony.SmsMessage;

/**
 * This class manages sending messages to other contacts.
 * @author Julio Mendoza
 */
public class MessageSender {

    /**
     * The text of the message to send.
     */
    private String messageText;

    /**
     * The number of the contact to call.
     */
    private String phoneNumber;



    /**
     * The message to send.
     */
    private SmsMessage message;

    /**
     * Initializes the instance.
     * @param messageText
     * @param phoneNumber
     */
    public MessageSender(String messageText, String phoneNumber)
    {
        this.messageText = messageText;
        this.phoneNumber = phoneNumber;
    }

    public MessageSender()
    {
        this.messageText = "";
        this.phoneNumber = "";
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }


    public void sendMessage() throws Exception
    {
        try
        {
            SmsManager manager = SmsManager.getDefault();
            manager.sendTextMessage(phoneNumber, null, messageText, null, null);

        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new Exception("Can't send message. "+ e.getMessage());
        }

    }
}
