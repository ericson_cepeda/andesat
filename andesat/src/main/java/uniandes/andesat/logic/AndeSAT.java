package uniandes.andesat.logic;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by User on 9/12/13.
 */
public class AndeSAT {

    /**
     * The current instance of the object.
     */
    private static AndeSAT instance;

    /**
     * The driver user role of the application.
     */
    private Driver driver;

    /**
     * The passenger role of the application.
     */
    private Passenger passenger;

    private ArrayList<Trip> trips;

    /**
     * Creates an instance of the object.
     */
    public AndeSAT()
    {
        trips = new ArrayList<Trip>();
    }

    /**
     * Returns the current instance of the object.
     * @return instance.
     */
    public static AndeSAT getInstance()
    {
        if(instance == null)
        {
            instance = new AndeSAT();
        }
        return instance;
    }

    /**
     * Creates a route and adds it to the driver's route list.
     * @param routeName the name of the route to add. Must be unique.
     * @param approxLenth The approximate length in minutes of the route.
     * @param source The starting point of the route. Must be an address or "University" if the
     *               route starts at the university.
     * @param end The starting point of the route. Must be an address or "University" if the
     *               route ends at the university.
     * @throws Exception if the route already exists in the driver's list.
     */
    public void createRoute(String routeName, int approxLenth, String source, String end) throws Exception
    {
        Route route = new Route(routeName, approxLenth, source, end);
        driver.addRoute(route);
        route = null;
    }

    /**
     * Removes the route with the specified name.
     * @param routeName The name of the route to remove.
     * @throws Exception If the route with the specified name does not exist.
     */
    public void removeRoute(String routeName) throws Exception
    {
        driver.removeRoute(routeName);
    }

    /**
     * Returns the driver profile of the user.
     * @return
     */
    public Driver getDriver() {
        return driver;
    }

    /**
     * Creates a trip associated to a route including the list of passengers the driver selected to take on the trip.
     *
     * @param routeName The name of the route associated to the trip.
     * @param passengers The list of passengers the driver is going to drive.
     * @param startingDate The time and Date the trip is going to start.
     * @throws Exception If there is an error creating the trip. e. g. Sending the SMS messages to the
     * passengers or if the route doesn't exist.
     */
    public void createTrip(String routeName, Passenger[] passengers, Date startingDate) throws Exception
    {
        driver.createTrip(routeName, passengers, startingDate);
    }

    /**
     * Called when the user enters the car. Sends SMS to the passengers that board the car after
     * the user, giving them relevant information about the trip.
     * @param trip The trip to join.
     * @throws Exception if the SMS message can not be sent.
     */
    public void joinTrip(Trip trip) throws Exception
    {
        passenger.joinTrip(trip);
    }

    public void setPassenger(Passenger passenger) {
        this.passenger = passenger;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public Passenger getPassenger()
    {
        return passenger;
    }

    public void addRoute(Route route) throws Exception
    {
        driver.addRoute(route);
    }

    public void addTrip(Trip trip)
    {
        trips.add(trip);
    }
}
