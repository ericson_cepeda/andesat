package uniandes.andesat.logic;

/**
 * Created by User on 9/11/13.
 */
public abstract class User {

    /**
     * The name of the user.
     */
    protected String name;

    /**
     * The phone number of the user.
     */
    protected String phoneNumber;

    /**
     * The e-mail address of the user.
     */
    protected String email;

    /**
     * The password of the user.
     */
    protected String password;

    /**
     * Initilizes the cobject.
     */
    public User()
    {

    }



    /**
     * Gives the name of the user.
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Changes the name of the user for the parameter
     * @param name the new name to set.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Returns the number of the user's phone.
     * @return phoneNumber
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * Changes the number of the user for the parameter.
     * @param phoneNumber The new number.
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * Gets the e-mail address of the user.
     * @return
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the user e-mail address with the parameter
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
