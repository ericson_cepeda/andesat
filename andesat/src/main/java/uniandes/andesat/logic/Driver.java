package uniandes.andesat.logic;

import android.util.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Represents a driver in the system.
 * @author Julio Mendoza
 */
public class Driver extends User{

    public static final String UNIVERSITY = "University";
    /**
     * The routes of the driver.
     */
    private ArrayList<Route> routes;

    /**
     * The model of the car.
     */
    private String carModel;

    /**
     * The plates of the car.
     */
    private String carPlates;

    /**
     * Creates an instance of the object.
     */
    public Driver()
    {
        super();
        routes = new ArrayList<Route>();
    }

    /**
     * Creates a trip with an associated route and notifies the passengers about it.
     * @param routeName The name of the route to associate with the trip.
     * @param passengers The array of passengers in the trip.
     * @param startingDate The starting date and time of the trip.
     */
    public void createTrip(String routeName, Passenger[] passengers, Date startingDate)
    {
        try
        {
            String smsBody, smsNumber = "";
            DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            String date =  format.format(startingDate);
            Passenger passenger, other;
            Passenger[] others;
            String when;
            MessageSender sender;
            Route route = getRouteByName(routeName);

            if(route.getSource().equals(UNIVERSITY))
            {
                for (int i = 0; i < passengers.length; i++)
                {
                    passenger = passengers[i];
                    others = getOthers(passengers, passenger);
                    smsNumber = passenger.getPhoneNumber();
                    smsBody =
                            "Sent From AndeSAT:\n" +
                                    "Hi: "+passenger.getName()+"\n" +
                                    "I am: "+this.getName()+"\n" +
                                    "We are going from the "+UNIVERSITY+".\n" +
                                    "At approximately: "+ date +"\n"+
                                    "My car is: "+this.getCarModel()+"\n" +
                                    "The plates are: "+this.getCarPlates()+"\n";

                    if(others != null && others.length > 0)
                    {
                        smsBody+= "Your trip partners are:\n";
                        for(int j = 0; j < others.length; j++)
                        {
                            other = others[i];
                            when = isBefore(passenger, other, passengers)? "Before":"After";
                            smsBody+= "Partner "+(j+1)+": "+other.getName()+"|"+other.getPhoneNumber()+"|"+when+"\n";
                        }
                    }
                    sender = new MessageSender(smsBody, smsNumber);
                    sender.sendMessage();
                }
            }
            else
            {
                for (int i = 0; i < passengers.length; i++)
                {
                    passenger = passengers[i];
                    others = getOthers(passengers, passenger);
                    smsNumber = passenger.getPhoneNumber();
                    smsBody =
                            "Sent From AndeSAT:\n" +
                                    "Hi: "+passenger.getName()+"\n" +
                                    "I am: "+this.getName()+" \n" +
                                    "We are leaving to the "+UNIVERSITY+".\n" +
                                    "At approximately: "+ date +"\n"+
                                    "My car is: "+this.getCarModel()+"\n" +
                                    "The plates are: "+this.getCarPlates()+"\n";

                    if(others != null && others.length > 0)
                    {
                        smsBody+="Your trip partners are:\n";
                        for(int j = 0; j < others.length; j++)
                        {
                            other = others[i];
                            when = isBefore(passenger, other, passengers)? "Before":"After";
                            smsBody+= "Partner "+(j+1)+": "+other.getName()+"|"+other.getPhoneNumber()+"|"+when+"\n";
                        }
                    }
                    sender = new MessageSender(smsBody, smsNumber);
                    sender.sendMessage();
                }
            }

            Trip trip = new Trip(startingDate, passengers, this.name, this.phoneNumber, this.carModel, this.carPlates);
            trip.setAssociatedRoute(route);
        }
        catch(Exception e)
        {
            e.printStackTrace();
            Log.d("AndeSAT", "Error creating the trip: "+e.getMessage());
        }
    }

    /**
     * Returns the list of passengers that are different from the one in the parameter list.
     * @param list The list of passengers to give the others.
     * @param passenger The passenger to compare from.
     * @return the list of passengers.
     */
    private Passenger[] getOthers (Passenger[] list, Passenger passenger)
    {
        Passenger[] others = new Passenger[list.length-1];
        Passenger x;
        for (int i = 0; i < list.length; i++)
        {
            x = list[i];
            if(!(x.getName().equals(passenger.getName()) || x.getPhoneNumber().equals(passenger.getPhoneNumber())))
            {
                others[i] = x;
            }
        }
        return others;
    }

    /**
     * <pre>a and b are included in the passengers list.</pre>
     * Determines if a passenger is on the route before the other.
     * @param a The passenger to comnpare from.
     * @param b The passenger to compare to.
     * @param passengers The list of passengers where both of the passengers are located.
     * @return <b>True</b> if passenger a is boarding before passenger b. <b>False</b> on the contrary.
     */
    private boolean isBefore(Passenger a, Passenger b, Passenger[] passengers)
    {
        boolean before = false;
        Passenger x, y;
        for(int i = 0; i < passengers.length; i ++)
        {
            x = passengers[i];
            if(!x.equals(a))
            {
                continue;
            }
            for (int j = 0; j < passengers.length; j ++)
            {
                if (j == i)
                {
                    continue;
                }
                y = passengers[j];
                if(!y.equals(b))
                {
                    continue;
                }
                if(i < j)
                {
                    before = true;
                }
            }
        }

        return before;
    }

    /**
     * Adds a route to the route list.
     * @param route The route to add.
     * @throws Exception If the route already exists.
     */
    public void addRoute(Route route) throws Exception
    {
        if(!routeExists(route))
        {
            routes.add(route);
        }
        else
        {
            throw new Exception("The route with name: "+route.getName()+" already exists.");
        }
    }

    /**
     * Detects if a route is equal to another route that was previously added.
     * @param route
     * @return
     */
    private boolean routeExists(Route route)
    {
        for (Route r: routes)
        {
            if(r.equals(route))
            {
                return true;
            }

        }
        return false;
    }

    /**
     * Removes the route with the specified name.
     * @param routeName The name of the route to remove.
     * @throws Exception If the route with the specified name does not exist.
     */
    public void removeRoute(String routeName) throws  Exception
    {
        Route r = getRouteByName(routeName);
        if(r!=null)
        {
            routes.remove(r);
        }
        else
        {
            throw new Exception("The route with the name "+routeName+ "doesn't exist :(");
        }
    }

    public Route getRouteByName(String routeName)
    {
        Route route = null;
        boolean found = false;
        Route r;
        for(int i = 0; i < routes.size()&&!found; i++)
        {
            r = routes.get(i);
            if(r.getName().equals(routeName))
            {
                route = r;
                found = true;
            }
        }
        return route;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public void setCarPlates(String carPlates) {
        this.carPlates = carPlates;
    }

    public String getCarModel() {
        return carModel;
    }

    public String getCarPlates() {
        return carPlates;
    }
}
