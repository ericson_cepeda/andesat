package uniandes.andesat.receivers;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import uniandes.andesat.R;
import uniandes.andesat.activities.MainActivity;
import uniandes.andesat.logic.MessageReader;

/**
 * @author Julio Mendoza.
 */
public class SMSReceiver extends BroadcastReceiver {


    public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            MessageReader reader;
            if (bundle != null){
                try{
                    Object[] pdus = (Object[]) bundle.get("pdus");
                    for (Object pdu : pdus) {
                        reader = MessageReader.getInstance();
                        if (reader.readMessage(pdu, context)) {
                            NotificationCompat.Builder mBuilder =
                                    new NotificationCompat.Builder(context)
                                            .setSmallIcon(R.drawable.ic_launcher)
                                            .setContentTitle("AndeSAT")
                                            .setContentText("New trip activity");
                            Intent resultIntent = new Intent(context, MainActivity.class);

                            TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
                            stackBuilder.addParentStack(MainActivity.class);
                            stackBuilder.addNextIntent(resultIntent);
                            PendingIntent resultPendingIntent =
                                    stackBuilder.getPendingIntent(
                                            0,
                                            PendingIntent.FLAG_UPDATE_CURRENT
                                    );
                            mBuilder.setContentIntent(resultPendingIntent);
                            NotificationManager mNotificationManager =
                                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                            mNotificationManager.notify(1, mBuilder.build());
                        }
                    }
                }catch(Exception e){
                    e.printStackTrace();
                }
            }


    }
}
